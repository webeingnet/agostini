<?php
/*
Template Name: Frontpage
*/

global $tpl;

//remove automatic paragraph in wordpress editor for the frontpage based on posts
remove_filter('the_content', 'wpautop');

//create arguments for custom loops
$args_first = array( 'tag' => get_option($tpl->name . '_first_tag', 'first-post'));
$args = array( 'tag' => get_option($tpl->name . '_featured_products_tag', 'product'));
$args_before = array('category_name' => 'frontpage', 'tag' => get_option($tpl->name . '_before_tag', 'before'), 'orderby' => 'date', 'order' => 'ASC');
$args_after = array('category_name' => 'frontpage', 'tag' => get_option($tpl->name . '_after_tag', 'after'), 'orderby' => 'date', 'order' => 'ASC');
$loop_first = new WP_Query( $args_first );
$loop_before = new WP_Query( $args_before );
$loop_after = new WP_Query( $args_after );

gk_load('header');

?>

<?php if ( have_posts() ) : ?>
		<?php while ( $loop_first->have_posts() ) : $loop_first->the_post(); ?>
			<div id="gk-head-frontpage">
				<div id="gk-header-mod" class="gk-page">
					<?php the_content(); ?>
				</div>
			</div>
		<?php endwhile; ?>
		<?php while ( $loop_before->have_posts() ) : $loop_before->the_post(); ?>	
			<div class="gk-container">
				<div class="gk-page">
				<?php the_content(); ?>
				</div>
			</div>	
		<?php endwhile; ?>
		
		<div id="gk-bottom2">
			<div class="gk-page widget-area">
				<div class="box centered">
					<?php query_posts('category_name=frontpage&tag=before_products'); ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; ?>
					<div class="gk-news-gallery">
						<div class="gk-images-wrapper gk-images-cols3">
							<?php query_posts($args); ?>
							<?php while ( have_posts() ) : the_post(); ?>
								
								<?php if(has_post_thumbnail()) : ?>
									<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>" class="gk-image show active">
										<?php the_post_thumbnail('product'); ?>
									</a>
								<?php endif; ?>
								
							<?php endwhile; ?>
						</div>
					</div>
					<?php query_posts('category_name=frontpage&tag=after_products'); ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; ?>
				</div>
			</div>
		</div>		
		
		<div id="gk-bottom-wrap">
		<?php while ( $loop_after->have_posts() ) : $loop_after->the_post(); ?>
				<div class="gk-bottom">
					<div class="gk-page">
						<?php the_content(); ?>
					</div>
				</div>
		<?php endwhile; ?>
		</div>
		
<?php else : ?>
	<section id="gk-mainbody">
		<article id="post-0" class="post no-results not-found">
			<header class="entry-header">
				<h1 class="entry-title"><?php _e( 'Nothing Found', GKTPLNAME ); ?></h1>
			</header>

			<div class="entry-content">
				<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', GKTPLNAME ); ?></p>
				<?php get_search_form(); ?>
			</div>
		</article>
	</section>
<?php endif; ?>
<?php wp_reset_query(); ?>
<?php
gk_load('footer');

// EOF