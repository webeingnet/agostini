<?php

function gk_installer_run($table_prefix, $wpdb) {
	// set the predefined options
	gk_installer_predefined_options($wpdb, $table_prefix);
	// page url
	$page_url = get_option('siteurl');
	// load the SQL dumps files
	$comments_dump = file_get_contents(dirname(__FILE__) . '/sql_dumps/comments.sql');
	$options_dump = file_get_contents(dirname(__FILE__) . '/sql_dumps/options.sql');
	$postmeta_dump = file_get_contents(dirname(__FILE__) . '/sql_dumps/postmeta.sql');
	$posts_dump = file_get_contents(dirname(__FILE__) . '/sql_dumps/posts.sql');
	$term_rel_dump = file_get_contents(dirname(__FILE__) . '/sql_dumps/term_relationships.sql');
	$term_tax_dump = file_get_contents(dirname(__FILE__) . '/sql_dumps/term_taxonomy.sql');
	$terms_dump = file_get_contents(dirname(__FILE__) . '/sql_dumps/terms.sql');
	
	// WC Tables	
	$woocommerce_attribute_taxonomies_dump = file_get_contents(dirname(__FILE__) . '/sql_dumps/woocommerce_attribute_taxonomies.sql');
	//$woocommerce_downloadable_product_permissions_dump = file_get_contents(dirname(__FILE__) . '/sql_dumps/woocommerce_downloadable_product_permissions.sql');
	$woocommerce_order_itemmeta_dump = file_get_contents(dirname(__FILE__) . '/sql_dumps/woocommerce_order_itemmeta.sql');
	$woocommerce_order_items_dump = file_get_contents(dirname(__FILE__) . '/sql_dumps/woocommerce_order_items.sql');
	$woocommerce_tax_rates_dump = file_get_contents(dirname(__FILE__) . '/sql_dumps/woocommerce_tax_rates.sql');
	//$woocommerce_tax_rate_locations_dump = file_get_contents(dirname(__FILE__) . '/sql_dumps/woocommerce_tax_rate_locations.sql');
	$woocommerce_termmeta_dump = file_get_contents(dirname(__FILE__) . '/sql_dumps/woocommerce_termmeta.sql');
	
	
	// replace all variables with the proper values
	$comments_dump = str_replace(array('{$table_prefix}', '{$page_url}'), array($table_prefix, $page_url), $comments_dump);
	$options_dump = str_replace(array('{$table_prefix}', '{$page_url}', '{$inactive_widgets}'), array($table_prefix, $page_url, 's:'.(16 + strlen($table_prefix)).':\"'.$table_prefix.'inactive_widgets'), $options_dump);
	$postmeta_dump = str_replace(array('{$table_prefix}', '{$page_url}'), array($table_prefix, $page_url), $postmeta_dump);
	$posts_dump = str_replace(array('{$table_prefix}', '{$page_url}'), array($table_prefix, $page_url), $posts_dump);
	$term_rel_dump = str_replace(array('{$table_prefix}', '{$page_url}'), array($table_prefix, $page_url), $term_rel_dump);
	$term_tax_dump = str_replace(array('{$table_prefix}', '{$page_url}'), array($table_prefix, $page_url), $term_tax_dump);
	$terms_dump = str_replace(array('{$table_prefix}', '{$page_url}'), array($table_prefix, $page_url), $terms_dump);
	
	
	// WC Tables
	$woocommerce_attribute_taxonomies_dump = str_replace(array('{$table_prefix}', '{$page_url}'), array($table_prefix, $page_url), $woocommerce_attribute_taxonomies_dump);
	//$woocommerce_downloadable_product_permissions_dump = str_replace(array('{$table_prefix}', '{$page_url}'), array($table_prefix, $page_url), $woocommerce_downloadable_product_permissions_dump);
	$woocommerce_order_itemmeta_dump = str_replace(array('{$table_prefix}', '{$page_url}'), array($table_prefix, $page_url), $woocommerce_order_itemmeta_dump);
	$woocommerce_order_items_dump = str_replace(array('{$table_prefix}', '{$page_url}'), array($table_prefix, $page_url), $woocommerce_order_items_dump);
	$woocommerce_tax_rates_dump = str_replace(array('{$table_prefix}', '{$page_url}'), array($table_prefix, $page_url), $woocommerce_tax_rates_dump);
	//$woocommerce_tax_rate_locations_dump = str_replace(array('{$table_prefix}', '{$page_url}'), array($table_prefix, $page_url), $woocommerce_tax_rate_locations_dump);
	$woocommerce_termmeta_dump = str_replace(array('{$table_prefix}', '{$page_url}'), array($table_prefix, $page_url), $woocommerce_termmeta_dump);
	
	
	// run the queries from SQL dumps
	$wpdb->query($comments_dump);
	$wpdb->query('ALTER TABLE '.$table_prefix.'comments AUTO_INCREMENT=65;');
	$wpdb->query($options_dump);
	$wpdb->query('ALTER TABLE '.$table_prefix.'options AUTO_INCREMENT=12708;');
	$wpdb->query($postmeta_dump);
	$wpdb->query('ALTER TABLE '.$table_prefix.'postmeta AUTO_INCREMENT=6843;');
	$wpdb->query($posts_dump);
	$wpdb->query('ALTER TABLE '.$table_prefix.'posts AUTO_INCREMENT=2418;');
	$wpdb->query($term_rel_dump);
	// no alter table for the term_relationships
	$wpdb->query($term_tax_dump);
	$wpdb->query('ALTER TABLE '.$table_prefix.'term_taxonomy AUTO_INCREMENT=172;');
	$wpdb->query($terms_dump);
	$wpdb->query('ALTER TABLE '.$table_prefix.'terms AUTO_INCREMENT=172;');
	
	
	
	
	// Create the WC Tables
	$wc1_dump = file_get_contents(dirname(__FILE__) . '/sql_dumps/wc1.sql');
	$wc1_dump = str_replace(array('{$table_prefix}', '{$page_url}'), array($table_prefix, $page_url), $wc1_dump);
	$wpdb->query($wc1_dump);
	$wc2_dump = file_get_contents(dirname(__FILE__) . '/sql_dumps/wc2.sql');
	$wc2_dump = str_replace(array('{$table_prefix}', '{$page_url}'), array($table_prefix, $page_url), $wc2_dump);
	$wpdb->query($wc2_dump);
	$wc3_dump = file_get_contents(dirname(__FILE__) . '/sql_dumps/wc3.sql');
	$wc3_dump = str_replace(array('{$table_prefix}', '{$page_url}'), array($table_prefix, $page_url), $wc3_dump);
	$wpdb->query($wc3_dump);
	$wc4_dump = file_get_contents(dirname(__FILE__) . '/sql_dumps/wc4.sql');
	$wc4_dump = str_replace(array('{$table_prefix}', '{$page_url}'), array($table_prefix, $page_url), $wc4_dump);
	$wpdb->query($wc4_dump);
	$wc5_dump = file_get_contents(dirname(__FILE__) . '/sql_dumps/wc5.sql');
	$wc5_dump = str_replace(array('{$table_prefix}', '{$page_url}'), array($table_prefix, $page_url), $wc5_dump);
	$wpdb->query($wc5_dump);
	$wc6_dump = file_get_contents(dirname(__FILE__) . '/sql_dumps/wc6.sql');
	$wc6_dump = str_replace(array('{$table_prefix}', '{$page_url}'), array($table_prefix, $page_url), $wc6_dump);
	$wpdb->query($wc6_dump);
	$wc7_dump = file_get_contents(dirname(__FILE__) . '/sql_dumps/wc7.sql');
	$wc7_dump = str_replace(array('{$table_prefix}', '{$page_url}'), array($table_prefix, $page_url), $wc7_dump);
	$wpdb->query($wc7_dump);
	
	
	// WC Tables
	$wpdb->query($woocommerce_attribute_taxonomies_dump);
	$wpdb->query('ALTER TABLE '.$table_prefix.'woocommerce_attribute_taxonomies AUTO_INCREMENT=3;');
	//$wpdb->query($woocommerce_downloadable_product_permissions_dump);
	//$wpdb->query('ALTER TABLE '.$table_prefix.'woocommerce_downloadable_product_permissions AUTO_INCREMENT=[#WC2#];');
	$wpdb->query($woocommerce_order_itemmeta_dump);
	$wpdb->query('ALTER TABLE '.$table_prefix.'woocommerce_order_itemmeta AUTO_INCREMENT=60;');
	$wpdb->query($woocommerce_order_items_dump);
	$wpdb->query('ALTER TABLE '.$table_prefix.'woocommerce_order_items AUTO_INCREMENT=8;');
	$wpdb->query($woocommerce_tax_rates_dump);
	$wpdb->query('ALTER TABLE '.$table_prefix.'woocommerce_tax_rates AUTO_INCREMENT=430;');
	//$wpdb->query($woocommerce_tax_rate_locations_dump);
	//$wpdb->query('ALTER TABLE '.$table_prefix.'woocommerce_tax_rate_locations AUTO_INCREMENT=[#WC6#];');
	$wpdb->query($woocommerce_termmeta_dump);
	$wpdb->query('ALTER TABLE '.$table_prefix.'woocommerce_termmeta AUTO_INCREMENT=92;');
}

function gk_installer_predefined_options($wpdb, $table_prefix) {
	// set the theme
	update_option('template', "Simplicity");
	update_option('stylesheet', "Simplicity");
}

// EOF
