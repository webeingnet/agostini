<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'agostini');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{9z|p|vK[Yrw%SIon;%+%WPEy#j(Pm[i)hHaHtB]z*QCGy/?*{Rht*I9BWs+IvPT');
define('SECURE_AUTH_KEY',  'C*?BXQUvOS{Ooq~bDep+|q$N7-EnZi?J=eOV;*Rr|m#X{V1SYOy++GJ0I>djsSx0');
define('LOGGED_IN_KEY',    's{)zO=A%Mw^l?C{s3az$C[BO9Z:[-wH?hKR{}hw-2Swb5PY&GUS[P{Z~P1r6iv$Z');
define('NONCE_KEY',        'hx^jevS_,K&n3)}()s.+MhA_fWX*w4q!fBjiER;Y*8MdIp%0Fq_~,$x.IX2h<iEt');
define('AUTH_SALT',        ';ZO+XT.-z+af) tT8S1>D54!LsXd,T3fV.Y=^@|6qz<6DT%no+03(]MP=4#+{*FJ');
define('SECURE_AUTH_SALT', '~oB>ScEFm?UFg>,CkL|;[x4DwJBY35zo|O8z|W0)CB;-fz^Cw?WW@-%ylsc]Jd^x');
define('LOGGED_IN_SALT',   'tnh,.W-{>P1-W4z/Q{XT^BKQq(-^^jk8j&Bx+`$>&~kt~kkcN }:4}5gf,j4@I@L');
define('NONCE_SALT',       'vpm40,z=mdH`WS#=0|}EOz_g6WhhZ:v,AK]dZlt:NOAZ8AgH-MD([5 ,=*j[=C]p');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
